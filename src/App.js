import avatar from "./assets/images/avatar.jpg";
import style from "./App.module.css";

function App() {
  return (
    <div className={style.devcampContainer}>
      <div>
        <img className={style.devcampAvatar} src={avatar} width={100} alt="avatar"/>
      </div>
      <div>
        <p className={style.devcampQuote}>This is one of the best developer blogs on the planet! I read it daily to improve my skills.</p>
      </div>
      <div>
        <p className={style.devcampInfo}><b>Tammy Stevens</b> * Front End Developer</p>
      </div>
    </div>
  );
}

export default App;
